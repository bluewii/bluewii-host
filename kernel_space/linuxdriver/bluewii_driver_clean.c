#include <linux/module.h>
#include <linux/init.h>

#include <linux/slab.h>      /* kmalloc() */
#include <linux/usb.h>      /* USB stuff */
#include <linux/mutex.h>    /* mutexes */
#include <linux/ioctl.h>

#include <asm/uaccess.h>    /* copy_*_user */


#define USB_VID  0x1941
#define USB_PID  0x8021


static inline void debug_data(const char *function, int size, const unsigned char *data);
static void ctrl_callback(struct urb *urb);
static void int_in_callback(struct urb *urb);

/*
 * Cleanup
 */
static void abort_transfers(struct usb_ml *dev);
static inline void delete(struct usb_ml *dev);

/*
 * Called when device is connected to the host.
 */
static int probe(struct usb_interface *interface, const struct usb_device_id *id);
/*
 * Called when device gets disconnected.
 */
static void disconnect(struct usb_interface *interface);

/*
 * open file syscall
 */
static int open(struct inode *inode, struct file *file);
/*
 * close file syscall
 */
static int release(struct inode *inode, struct file *file);
/*
 * write file syscall
 */
static ssize_t write(struct file *file, const char __user *user_buf, size_t count, loff_t *ppos);

static struct file_operations file_ops = {
  .owner = THIS_MODULE,
  .write = write,
  .open = open,
  .release = release,
};

static struct usb_class_driver file_class = {
  .name = "bluewii%d",
  .fops = &file_ops,
  .minor_base = ML_MINOR_BASE,
};

// __init and __exit are macros that put this code in a specific memory section
// through __attribute__((__section__(".init.text")))
// More in include/linux/init.h .
/*
 * Called on module loading.
 */
static int __init usb_ml_init(void);
/*
 * Called on module unloading.
 */
static void __exit usb_ml_exit(void);
