#include "pch.h"

#include <stdio.h>

LONG __cdecl
_tmain(
    LONG     Argc,
    LPTSTR * Argv
    )
/*++

Routine description:

    Sample program that communicates with a USB device using WinUSB

--*/
{
    DEVICE_DATA           deviceData;
    HRESULT               hr;
    USB_DEVICE_DESCRIPTOR deviceDesc;
    BOOL                  bResult;
    BOOL                  noDevice;
    ULONG                 lengthReceived;

    UNREFERENCED_PARAMETER(Argc);
    UNREFERENCED_PARAMETER(Argv);

    //SetCursorPos(100, 100);
    /*
    UINT SendInput( UINT    cInputs, LPINPUT pInputs, int     cbSize );
    typedef struct tagINPUT {
      DWORD type;
      union {
        MOUSEINPUT    mi;
        KEYBDINPUT    ki;
        HARDWAREINPUT hi;
      } DUMMYUNIONNAME;
    } INPUT, *PINPUT, *LPINPUT;
    typedef struct tagMOUSEINPUT {
      LONG      dx;
      LONG      dy;
      DWORD     mouseData;
      DWORD     dwFlags;
      DWORD     time;
      ULONG_PTR dwExtraInfo;
    } MOUSEINPUT, *PMOUSEINPUT, *LPMOUSEINPUT;
    typedef struct tagKEYBDINPUT {
      WORD      wVk;
      WORD      wScan;
      DWORD     dwFlags;
      DWORD     time;
      ULONG_PTR dwExtraInfo;
    } KEYBDINPUT, *PKEYBDINPUT, *LPKEYBDINPUT;
    typedef struct tagHARDWAREINPUT {
      DWORD uMsg;
      WORD  wParamL;
      WORD  wParamH;
    } HARDWAREINPUT, *PHARDWAREINPUT, *LPHARDWAREINPUT;
    */
    MOUSEINPUT mouseInput = {};
    mouseInput.dx = 32000;
    mouseInput.dy = 32000;
    mouseInput.dwFlags = MOUSEEVENTF_MOVE | MOUSEEVENTF_ABSOLUTE;
    INPUT mInput;
    mInput.type = 0;
    mInput.mi = mouseInput;

    KEYBDINPUT keybdInput0 = {};
    keybdInput0.wVk = VK_CONTROL;
    KEYBDINPUT keybdInput1 = {};
    keybdInput1.wVk = 0x53;
    INPUT kInput0 = {};
    kInput0.type = 1;
    kInput0.ki = keybdInput0;
    INPUT kInput1 = {};
    kInput1.type = 1;
    kInput1.ki = keybdInput1;

    SendInput(1, &mInput, sizeof(mInput));
    SendInput(1, &kInput0, sizeof(kInput0));
    SendInput(1, &kInput1, sizeof(kInput1));
    kInput0.ki.dwFlags = KEYEVENTF_KEYUP;
    kInput1.ki.dwFlags = KEYEVENTF_KEYUP;
    SendInput(1, &kInput0, sizeof(kInput0));
    SendInput(1, &kInput1, sizeof(kInput1));


    /*
    INPUT inputs[4];
    inputs[0] = mInput;
    inputs[1] = kInput0;
    inputs[2] = kInput1;
    */


    //
    // Find a device connected to the system that has WinUSB installed using our
    // INF
    //
    hr = OpenDevice(&deviceData, &noDevice);

    if (FAILED(hr)) {

        if (noDevice) {

            wprintf(L"Device not connected or driver not installed\n");

        } else {

            wprintf(L"Failed looking for device, HRESULT 0x%x\n", hr);
        }

        return 0;
    }

    //
    // Get device descriptor
    //
    bResult = WinUsb_GetDescriptor(deviceData.WinusbHandle,
                                   USB_DEVICE_DESCRIPTOR_TYPE,
                                   0,
                                   0,
                                   (PBYTE) &deviceDesc,
                                   sizeof(deviceDesc),
                                   &lengthReceived);

    if (FALSE == bResult || lengthReceived != sizeof(deviceDesc)) {

        wprintf(L"Error among LastError %d or lengthReceived %d\n",
                FALSE == bResult ? GetLastError() : 0,
                lengthReceived);
        CloseDevice(&deviceData);
        return 0;
    }

    //
    // Print a few parts of the device descriptor
    //
    wprintf(L"Device found: VID_%04X&PID_%04X; bcdUsb %04X\n",
            deviceDesc.idVendor,
            deviceDesc.idProduct,
            deviceDesc.bcdUSB);

    CloseDevice(&deviceData);
    return 0;
}
