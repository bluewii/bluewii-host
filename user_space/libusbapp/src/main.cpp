#include <windows.h>
#include <stdio.h>
#include <string.h>

#include "libusb.h"

#if defined(_MSC_VER) && (_MSC_VER < 1900)
#define snprintf _snprintf
#endif

int main()
{
  int result = libusb_init(NULL);

  libusb_device** devs;
  int cnt = 0;

  cnt = libusb_get_device_list(NULL, &devs);
  
  printf("Len of devices list: %d", cnt);

  libusb_free_device_list(devs, 1);
  libusb_exit(NULL);
  return 0;
}
