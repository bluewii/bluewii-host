@echo off

mkdir build
pushd build
cl -DDEBUG /Zi ..\src\main.cpp /I "..\libusb\include\libusb-1.0" /link /LIBPATH:"..\libusb\MS64\static"  libusb-1.0.lib advapi32.lib
popd
