#include <windows.h>
#include <hidsdi.h>
#include <setupapi.h>
#include <wchar.h>
#include <stdio.h>


void PrintGUID(GUID guid)
{
  wprintf(L"  - HID GUID: %x %x %x ", guid.Data1, guid.Data2, guid.Data3);
  for (int i = 0; i < 8; ++i)
  {
    wprintf(L"%x", (wchar_t)guid.Data4[i]);
  }
  wprintf(L"\n");
}

int main()
{
  wprintf(L" >> Starting custom Game Pad HID driver...\n");
  GUID hidGUID = {};
  HidD_GetHidGuid(&hidGUID);

  PrintGUID(hidGUID);

  WCHAR pnpID[64] = L"USB\\VID_0079&PID_0011\\DEMO";

  // Device information set consists of device information elements for all
  // the devices that belong to some device setup class or device interface class.
  HDEVINFO hwDevInfo = {};

  hwDevInfo = SetupDiGetClassDevsW(
    &hidGUID, NULL, NULL,
    (DIGCF_PRESENT | DIGCF_DEVICEINTERFACE)
  ); 

  if (hwDevInfo == INVALID_HANDLE_VALUE)
  {
    wprintf(L"  >E Error after SetupDiGetClassDevsW(): %d", GetLastError());
  }


  SP_DEVICE_INTERFACE_DATA deviceInterfaceData = {};
  deviceInterfaceData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
  SP_DEVICE_INTERFACE_DETAIL_DATA interfaceDetailData = {};
  interfaceDetailData.cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
  SP_DEVINFO_DATA deviceInfoData = {};
  deviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);

  BOOL result = TRUE;
  int interfaceIndex = 0;
  BOOL lastInterface = FALSE;
  BOOL foundTheDevice = FALSE;
  HANDLE file = INVALID_HANDLE_VALUE;

  while (1)
  {
    result = SetupDiEnumDeviceInterfaces(
      hwDevInfo,
      NULL,       // Not using specific PDOs (physical device object)
      &hidGUID,
      interfaceIndex++,
      &deviceInterfaceData
    );

    // Open the device interface and Check if it is our device
    // by matching the Usage page and Usage from Hid_Caps.
    // If this is our device then send the hid request.
    if (OpenDeviceInterface(hwDevInfo, &deviceInterfaceData, &file))
    {
      foundTheDevice = TRUE;
      break;
    }

    PrintGUID(deviceInterfaceData.InterfaceClassGuid);

    if (result == FALSE)
    {
      if (GetLastError() == ERROR_NO_MORE_ITEMS)
      {
        lastInterface = TRUE;
      }
      else
      {
        wprintf(L"  >E Error after SetupDiEnumDeviceInterfaces(): %d", GetLastError());
        break;
      }
    }

    result = SetupDiGetDeviceInterfaceDetailA(
      hwDevInfo,
      &deviceInterfaceData,
      &interfaceDetailData,
      256,  // TODO(michalc): hack, ogarnij to
      NULL,
      &deviceInfoData
    );

    printf("     - %s\n", interfaceDetailData.DevicePath);

    if (lastInterface)
    {
      break;
    }

    if (result == FALSE)
    {
      wprintf(L"  >E Error after SetupDiGetDeviceInterfaceDetailA(): %d", GetLastError());
      break;
    }
  }


  /*
  HANDLE usbDevice;

  CreateFileA(
    LPCSTR                lpFileName,
    DWORD                 dwDesiredAccess,
    DWORD                 dwShareMode,
    LPSECURITY_ATTRIBUTES lpSecurityAttributes,
    DWORD                 dwCreationDisposition,
    DWORD                 dwFlagsAndAttributes,
    HANDLE                hTemplateFile
  );
  */
}
