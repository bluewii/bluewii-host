#include <stdio.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>
#include <stdint.h>
#include <math.h>

#define M_PI 3.14159265358979323846


#define global_variable static
#define internal        static
#define local_persist   static

typedef bool bool32;

typedef int8_t int8;
typedef int16_t int16;
typedef int32_t int32;
typedef int64_t int64;

typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;

typedef float real32;
typedef double real64;

typedef struct Controls
{
  bool32 lmbState;
  bool32 rmbState;
  int32 mouseX;
  int32 mouseY;
  int32 mouseXRelative;
  int32 mouseYRelative;
} Controls;

global_variable Controls controls;
global_variable bool32 running;

int main()
{
  Display *display = XOpenDisplay(0);
  running = true;

  Window root_window;
  Window rootReturn;
  Window childReturn;
  uint32 maskReturn;
  XWindowAttributes windowAttributes = {};

  root_window = XRootWindow(display, 0);
  XGetWindowAttributes(display, root_window, &windowAttributes);
  bool32 failure = XQueryPointer(display, root_window,
                                 &rootReturn, &childReturn,
                                 &(controls.mouseX), &(controls.mouseY),
                                 &(controls.mouseXRelative), &(controls.mouseYRelative),
                                 &maskReturn);
  printf("Root window size: %d %d\n",
         windowAttributes.width, windowAttributes.height);
  printf("Initial pointer pos: %d %d\n",
         controls.mouseX, controls.mouseY);
  printf("Initial relative pointer pos: %d %d\n",
         controls.mouseXRelative, controls.mouseYRelative);

  while (running)
  {
    root_window = XRootWindow(display, 0);
    XSelectInput(display, root_window, KeyReleaseMask);
    XWarpPointer(display, None, root_window, 0, 0, 0, 0, 100, 100);
    XSync(display, False);

    bool32 failure = XQueryPointer(display, root_window,
                                   &rootReturn, &childReturn,
                                   &(controls.mouseX), &(controls.mouseY),
                                   &(controls.mouseXRelative), &(controls.mouseYRelative),
                                   &maskReturn);
    if (failure == false) {
        break;
    }
  } // end of while(running)
}
