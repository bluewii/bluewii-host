if [ ! -d "build" ]; then
  mkdir build
fi
pushd build
g++ -g -Wall ../main.cpp $(pkg-config --cflags --libs x11) -o main
popd

