@echo off

mkdir build
pushd build
cl -DDEBUG /Zi ..\src\main.cpp /I "..\hidapi" /link /LIBPATH:"..\hidapi\MS64" User32.lib hidapi.lib
popd
