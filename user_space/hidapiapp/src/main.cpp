#include <stdio.h>
#include <signal.h>
#include <stdint.h>

#include <math.h>

#ifndef M_PI
#define M_PI   3.14159265358979323846264338327950288
#endif

#include "hidapi.h"

#ifdef _WIN32
  #include <windows.h>
#else
  #include <unistd.h>
  #include <wchar.h>
  #include <string.h>
  #include <stdlib.h>
#endif

#define global_variable static
#define local_persistent static
#define internal static

#include "util.cpp"
#include "ctrlr_handling.cpp"


#define DEBUG_OUTPUT


#define VID     0x0079
#define PID     0x0011


global_variable int running = 1;

void
SigHandler (int singal)
{
  printf(" >> SigHandler()");
  running = 0;
}


global_variable LARGE_INTEGER frequency;

int main(int argc, char* argv[])
{
  signal(SIGINT, SigHandler);
  signal(SIGTERM, SigHandler);

#ifdef WIN32
  UNREFERENCED_PARAMETER(argc);
  UNREFERENCED_PARAMETER(argv);
#endif

  QueryPerformanceFrequency(&frequency);
  HANDLE f_perf = CreateFileA("../logs/perf.csv",
                              GENERIC_WRITE,
                              0, NULL,
                              CREATE_ALWAYS,
                              FILE_ATTRIBUTE_NORMAL,
                              NULL);
  HANDLE f_stick_pos = CreateFileA("../logs/stick_pos.csv",
                                   GENERIC_WRITE,
                                   0, NULL,
                                   CREATE_ALWAYS,
                                   FILE_ATTRIBUTE_NORMAL,
                                   NULL);
  char f_perf_buffer[32] = {};
  char f_stick_pos_buffer[48] = {};

  int result;
  hid_device* usb_handle;

  if (hid_init())
    return -1;

  unsigned char usb_data_buffer[256];
  // Set up the command buffer.
  memset(usb_data_buffer, 0x00, sizeof(usb_data_buffer));
  usb_data_buffer[0] = 0x01;
  usb_data_buffer[1] = 0x81;

  // Open the device using the VID, PID,
  // and optionally the Serial number.
  usb_handle = hid_open(VID, PID, NULL);
  if (!usb_handle) {
    printf(" >> Unable to open device\n");
    return 1;
  }

  print_usb_info(usb_handle);
  hid_set_nonblocking(usb_handle, 0);

  // Request state (cmd 0x81). The first byte is the report number (0x1).
  usb_data_buffer[0] = 0x00;
  usb_data_buffer[1] = 0x81;
  result = hid_write(usb_handle, usb_data_buffer, 17);
  if (result < 0)
    printf(" >> Unable to write() (2)\n");

  hid_read(usb_handle, usb_data_buffer, sizeof(usb_data_buffer));
  drop_controller_data(&controller, usb_data_buffer);
  controller.stick_dir_prev = controller.stick_dir;
  controller.btn_C_prev = controller.btn_C;
  controller.btn_Z_prev = controller.btn_Z;

  controller_map.dir_N = 0x41;
  controller_map.dir_NE =0x42; 
  controller_map.dir_E = 0x43;
  controller_map.dir_SE = 0x44;
  controller_map.dir_S = 0x45;
  controller_map.dir_SW = 0x46;
  controller_map.dir_W = 0x47;
  controller_map.dir_NW = 0x48;
  controller_map.btn_C = VK_CONTROL;
  controller_map.btn_Z = 0;

  while (running) {
    LARGE_INTEGER counter_loop_start;
    LARGE_INTEGER counter_loop_end;
    LARGE_INTEGER counter_usb_poll_start;
    LARGE_INTEGER counter_usb_poll_end;

    QueryPerformanceCounter(&counter_loop_start);

    QueryPerformanceCounter(&counter_usb_poll_start);
    result = hid_read(usb_handle, usb_data_buffer, sizeof(usb_data_buffer));
    if (result == 0)
      printf(" >> waiting...\n");
    if (result < 0)
      printf(" >> Unable to read()\n");
    QueryPerformanceCounter(&counter_usb_poll_end);

    drop_controller_data(&controller, usb_data_buffer);

    MOUSEINPUT mouseInput = {};
    mouseInput.dwFlags = MOUSEEVENTF_MOVE;
    INPUT mInput;
    mInput.type = 0;

    KEYBDINPUT keybdInput[5] = {0};
    INPUT kInput[5] = {0};
    kInput[0].type = 1;
    kInput[1].type = 1;
    kInput[2].type = 1;
    kInput[3].type = 1;
    kInput[4].type = 1;

    uint8_t sent_events = 0;

    if (translate_controller(&mouseInput, keybdInput, &controller, STICK_KEYBD))
    {
      mInput.mi = mouseInput;
      sent_events = SendInput(1, &mInput, sizeof(INPUT));
      kInput[0].ki = keybdInput[0];
      kInput[1].ki = keybdInput[1];
      kInput[2].ki = keybdInput[2];
      kInput[3].ki = keybdInput[3];
      kInput[4].ki = keybdInput[4];
      sent_events += SendInput(5, kInput, sizeof(INPUT));
    };

#ifdef DEBUG_OUTPUT
    printf("%02d ", controller.stick_dir);
    printf("%02.03f ", controller.angle);
    printf("%02.03f ", controller.radius);
    printf("\n");
#endif
    
    controller.stick_dir_prev = controller.stick_dir;
    controller.btn_C_prev = controller.btn_C;

    QueryPerformanceCounter(&counter_loop_end);
    int measurement_1 = (1000000 * (counter_loop_end.QuadPart - counter_loop_start.QuadPart)/frequency.QuadPart);
    int measurement_2 = (1000000 * (counter_usb_poll_end.QuadPart - counter_usb_poll_start.QuadPart)/frequency.QuadPart);
    sprintf(f_perf_buffer, "%5i, %5i\n", measurement_1, measurement_2);
    sprintf(f_stick_pos_buffer, "%3i %3i %.2f %.2f\n",
            controller.stick_raw_x, controller.stick_raw_y,
            controller.radius, controller.angle);
    WriteFile(f_perf, f_perf_buffer, strlen(f_perf_buffer), NULL, NULL);
    WriteFile(f_stick_pos, f_stick_pos_buffer, strlen(f_stick_pos_buffer), NULL, NULL);
  }

  CloseHandle(f_perf);
  CloseHandle(f_stick_pos);

  hid_close(usb_handle);
  hid_exit();

  return 0;
}



