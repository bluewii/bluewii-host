#define DIR_NONE    0
#define DIR_N     0b00000001
#define DIR_E     0b00000010
#define DIR_S     0b00000100
#define DIR_W     0b00001000

#define DEADZONE_RADIUS 100.0f
//#define FULL_DIRECTIONS

enum CtrlrMode_e {
  STICK_KEYBD,
  STICK_PIE_BINARY,
  STICK_PIE_SMOOTH,
};

typedef struct _Controller_t
{
  uint8_t stick_raw_x;
  uint8_t stick_raw_y;
  uint8_t stick_dir;
  uint8_t stick_dir_prev;
  uint8_t btn_C;
  uint8_t btn_C_prev;
  uint8_t btn_Z;
  uint8_t btn_Z_prev;
  float radius;
  float angle;
} Controller_t;

typedef struct _Controller_map_t
{
  uint8_t dir_N;
  uint8_t dir_NE;
  uint8_t dir_E;
  uint8_t dir_SE;
  uint8_t dir_S;
  uint8_t dir_SW;
  uint8_t dir_W;
  uint8_t dir_NW;
  uint8_t btn_C;
  uint8_t btn_Z;
} Controller_map_t;


void drop_controller_data(Controller_t* ctrlr, uint8_t* data);
void get_binary_directions(Controller_t* ctrlr);
int cursor_keybd(KEYBDINPUT* keybdInput, Controller_t* ctrlr, Controller_map_t ctrlr_map);
int cursor_pie_smooth(MOUSEINPUT* mouseInput, int stick_raw_x, int stick_raw_y);
int cursor_pie_offset(MOUSEINPUT* mouseInput, int dir, int dir_previous);
int translate_controller(MOUSEINPUT* mI, KEYBDINPUT* kI, Controller_t* ctrlr, CtrlrMode_e mode);

global_variable Controller_t controller;
global_variable Controller_map_t controller_map;


void drop_controller_data(Controller_t* ctrlr, uint8_t* data)
{
  ctrlr->stick_dir = DIR_NONE;
  ctrlr->stick_raw_x = data[0];
  ctrlr->stick_raw_y = data[1];
  ctrlr->btn_C = (data[2] & 0b00000010) >> 1;
  ctrlr->btn_Z =  data[2] & 0b00000001;
}

void get_binary_directions(Controller_t* ctrlr)
{
  // TODO(michalc): this whole function should get optimized. Maybe lookup table for all those 
  //                values, computed at the start?
  float pi_over_4 = 0.785398163;
  float pi_over_2 = 0.3926990815;
  float angle = ctrlr->angle;

  if (ctrlr->radius > DEADZONE_RADIUS)
  {
#ifdef FULL_DIRECTIONS
    // Upper half circle
    if ( (angle > 0.0f) && (angle <= (pi_over_4 - pi_over_4/2.0f)) )
    {
      ctrlr->stick_dir = DIR_E;
    }
    else if ( (angle > (pi_over_4 - pi_over_4/2.0f)) && (angle <= (pi_over_4 + pi_over_4/2.0f)) )
    {
      ctrlr->stick_dir = (DIR_N | DIR_E);
    }
    else if ( (angle > (pi_over_4 + pi_over_4/2.0f)) && (angle <= (2*pi_over_4 + pi_over_4/2.0f)) )
    {
      ctrlr->stick_dir = DIR_N;
    }
    else if ( (angle > (2*pi_over_4 + pi_over_4/2.0f)) && (angle <= (3*pi_over_4 + pi_over_4/2.0f)) )
    {
      ctrlr->stick_dir = (DIR_N | DIR_W);
    }
    else if ( (angle > (3*pi_over_4 + pi_over_4/2.0f)) && (angle <= (4*pi_over_4 + pi_over_4/2.0f)) )
    {
      ctrlr->stick_dir = DIR_W;
    }
    // Lower half circle
    else if ( (angle < 0.0f) && (angle >= -(pi_over_4 - pi_over_4/2.0f)))
    {
      ctrlr->stick_dir = DIR_E;
    }
    else if ( (angle < -(pi_over_4 - pi_over_4/2.0f)) && (angle >= -(pi_over_4 + pi_over_4/2.0f)) )
    {
      ctrlr->stick_dir = (DIR_S | DIR_E);
    }
    else if ( (angle < -(pi_over_4 + pi_over_4/2.0f)) && (angle >= -(2*pi_over_4 + pi_over_4/2.0f)) )
    {
      ctrlr->stick_dir = DIR_S;
    }
    else if ( (angle < -(2*pi_over_4 + pi_over_4/2.0f)) && (angle >= -(3*pi_over_4 + pi_over_4/2.0f)) )
    {
      ctrlr->stick_dir = (DIR_S | DIR_W);
    }
    else if ( (angle < -(3*pi_over_4 + pi_over_4/2.0f)) && (angle >= -(4*pi_over_4 + pi_over_4/2.0f)) )
    {
      ctrlr->stick_dir = DIR_W;
    }
#else
    // Upper half circle
    if ( (angle > 0.0f) && (angle <= (pi_over_2 - pi_over_2/2.0f)) )
    {
      ctrlr->stick_dir = DIR_E;
    }
    else if ( (angle > (pi_over_2 - pi_over_2/2.0f)) && (angle <= (pi_over_2 + pi_over_2/2.0f)) )
    {
      ctrlr->stick_dir = DIR_N;
    }
    else if ( (angle > (angle <= (pi_over_2 + pi_over_2/2.0f))) && (angle <= (2*pi_over_2)) )
    {
      ctrlr->stick_dir = DIR_W;
    }
    // Lower half circle
    else if ( (angle < 0.0f) && (angle >= -(pi_over_2 - pi_over_2/2.0f)))
    {
      ctrlr->stick_dir = DIR_E;
    }
    else if ( (angle < -(pi_over_2 - pi_over_2/2.0f)) && (angle >= -(pi_over_2 + pi_over_2/2.0f)) )
    {
      ctrlr->stick_dir = DIR_S;
    }
    else if ( (angle < -(pi_over_2 + pi_over_2/2.0f)) && (angle >= -(-2*pi_over_2)) )
    {
      ctrlr->stick_dir = DIR_W;
    }
#endif
  }
}

int translate_controller(MOUSEINPUT* mI,
                         KEYBDINPUT* kI,
                         Controller_t* ctrlr,
                         CtrlrMode_e mode)
{
  float x = ctrlr->stick_raw_x - 127.5f;
  float y = ctrlr->stick_raw_y - 127.5f;
  cartesian_to_polar(x, y, &(ctrlr->angle), &(ctrlr->radius));

  get_binary_directions(ctrlr);

  // Since the deadzone also should use polar coordinates I'm using a post fix for the invalid values.
  if ((ctrlr->radius) < DEADZONE_RADIUS)
  {
    ctrlr->angle = 0;
    ctrlr->radius = 0;
  }

  int result = 0;
  switch (mode) {
    case STICK_KEYBD: {
             result = cursor_keybd(kI, ctrlr, controller_map);
             break;
           }
    case STICK_PIE_BINARY: {
             result = cursor_pie_offset(mI, ctrlr->stick_dir, ctrlr->stick_dir_prev);
             break;
           }
    case STICK_PIE_SMOOTH: {
             result = cursor_pie_smooth(mI, ctrlr->stick_raw_x, ctrlr->stick_raw_y);
             break;
           }
    default: {
               break;
             }
  }
  return result;
}

int cursor_keybd(KEYBDINPUT* keybdInput, Controller_t* ctrlr, Controller_map_t ctrlr_map)
{
  uint8_t dir = ctrlr->stick_dir;
  uint8_t dir_previous = ctrlr->stick_dir_prev;
  // This function has a state. Not the best idea.
  int button_pressed = 0;
  uint8_t input_change = 0;
  if (dir != dir_previous)
  {
    if (dir_previous == DIR_NONE)
    {
      if (dir == DIR_N)
      {
        button_pressed = ctrlr_map.dir_N;
      }
      if (dir == DIR_S)
      {
        button_pressed = ctrlr_map.dir_S;
      }
      if (dir == DIR_W)
      {
        button_pressed = ctrlr_map.dir_W;
      }
      if (dir == DIR_E)
      {
        button_pressed = ctrlr_map.dir_E;
      }
      if (dir == (DIR_N | DIR_E))
      {
        button_pressed = ctrlr_map.dir_NE;
      }
      if (dir == (DIR_N | DIR_W))
      {
        button_pressed = ctrlr_map.dir_NW;
      }
      if (dir == (DIR_S | DIR_E))
      {
        button_pressed = ctrlr_map.dir_SE;
      }
      if (dir == (DIR_S | DIR_W))
      {
        button_pressed = ctrlr_map.dir_SW;
      }

      keybdInput[4].wVk = button_pressed;

      input_change = 1;
    }

    keybdInput[4].wVk = button_pressed;
  }

  if ((ctrlr->btn_C_prev) && (!ctrlr->btn_C))
  {
    keybdInput[0].wVk = ctrlr_map.btn_C;
    input_change = 1;
  }
  else if ((!ctrlr->btn_C_prev) && (ctrlr->btn_C))
  {
    keybdInput[0].wVk = ctrlr_map.btn_C;
    keybdInput[0].dwFlags = KEYEVENTF_KEYUP;
    input_change = 1;
  }

  return input_change;
}

int cursor_pie_smooth(MOUSEINPUT* mouseInput, int stick_raw_x, int stick_raw_y)
{
  int pie_radius = 100;
  mouseInput->dwFlags |= MOUSEEVENTF_ABSOLUTE;
  POINT cursor_pos = {};
  GetCursorPos(&cursor_pos);

  int x = stick_raw_x - 128;
  int y = stick_raw_y - 128;
  float offset_magnitude = sqrt(x*x + y*y);

  if (offset_magnitude > 3.0f)
  {
    float offset_x = (stick_raw_x/256.0f) * pie_radius;
    float offset_y = (stick_raw_y/256.0f) * pie_radius;

    mouseInput->dx = ((cursor_pos.x + offset_x)/1920.0f) * 65535;
    mouseInput->dy = ((cursor_pos.y + offset_y)/1080.0f) * 65535;

    return 1;
  }

  return 0;
}

int cursor_pie_offset(MOUSEINPUT* mouseInput, int dir, int dir_previous)
{
  int pie_radius = 100;
  mouseInput->dwFlags |= MOUSEEVENTF_ABSOLUTE;
  local_persistent POINT cursor_pos = {};
  int offset_x = 0;
  int offset_y = 0;
  // Change from the center to wherever>
  if (dir_previous == DIR_NONE)
  {
    if (dir != dir_previous)
    {
      GetCursorPos(&cursor_pos);

      if (dir & DIR_N)
      {
        offset_y = -1 * pie_radius;
      }
      if (dir & DIR_S)
      {
        offset_y = 1 * pie_radius;
      }
      if (dir & DIR_W)
      {
        offset_x = -1 * pie_radius;
      }
      if (dir & DIR_E)
      {
        offset_x = 1 * pie_radius;
      }

      if (
        (dir == (DIR_N | DIR_E)) ||
        (dir == (DIR_N | DIR_W)) ||
        (dir == (DIR_S | DIR_E)) ||
        (dir == (DIR_S | DIR_W))
        )
      {
        offset_x *= 0.707f;
        offset_y *= 0.707f;
      }

      mouseInput->dx = ((cursor_pos.x + offset_x)/1920.0f) * 65535;
      mouseInput->dy = ((cursor_pos.y + offset_y)/1080.0f) * 65535;
      return 1;
    }
  }
  // Change from wherever...
  else if (dir_previous != DIR_NONE)
  {
    // ... to the center.
    if (dir == DIR_NONE)
    {
      mouseInput->dx = ((cursor_pos.x + offset_x)/1920.0f) * 65535;
      mouseInput->dy = ((cursor_pos.y + offset_y)/1080.0f) * 65535;
      return 1;
    }
    // ... to wherever else.
    else
    {
      if (dir & DIR_N)
      {
        offset_y = -100;
      }
      if (dir & DIR_S)
      {
        offset_y = 100;
      }
      if (dir & DIR_W)
      {
        offset_x = -100;
      }
      if (dir & DIR_E)
      {
        offset_x = 100;
      }

      mouseInput->dx = ((cursor_pos.x + offset_x)/1920.0f) * 65535;
      mouseInput->dy = ((cursor_pos.y + offset_y)/1080.0f) * 65535;
      return 1;

    }
  }

  return 0;
}
