void print_usb_info(hid_device* handle)
{
  int result;
  #define MAX_STR 255
  wchar_t wstr[MAX_STR];

  // Read the Manufacturer String
  wstr[0] = 0x0000;
  result = hid_get_manufacturer_string(handle, wstr, MAX_STR);
  if (result < 0)
    printf(" >> Unable to read manufacturer string\n");
  printf(" -- Manufacturer String: %ls\n", wstr);

  // Read the Product String
  wstr[0] = 0x0000;
  result = hid_get_product_string(handle, wstr, MAX_STR);
  if (result < 0)
    printf(" >> Unable to read product string\n");
  printf(" -- Product String: %ls\n", wstr);

  // Read the Serial Number String
  wstr[0] = 0x0000;
  result = hid_get_serial_number_string(handle, wstr, MAX_STR);
  if (result < 0)
    printf(" >> Unable to read serial number string\n");
  printf(" -- Serial Number String: (%d) %ls", wstr[0], wstr);
  printf("\n");

  // Read Indexed String 1
  wstr[0] = 0x0000;
  result = hid_get_indexed_string(handle, 1, wstr, MAX_STR);
  if (result < 0)
    printf(" >> Unable to read indexed string 1\n");
  printf(" -- Indexed String 0: %ls\n", wstr);
  // Read Indexed String 2
  wstr[0] = 0x0000;
  result = hid_get_indexed_string(handle, 2, wstr, MAX_STR);
  if (result < 0)
    printf(" >> Unable to read indexed string 1\n");
  printf(" -- Indexed String 1: %ls\n", wstr);
  // Read Indexed String 3
  wstr[0] = 0x0000;
  result = hid_get_indexed_string(handle, 3, wstr, MAX_STR);
  if (result < 0)
    printf(" >> Unable to read indexed string 1\n");
  printf(" -- Indexed String 2: %ls\n", wstr);
}

void cartesian_to_polar(float x, float y, float* angle, float* radius)
{
	*angle = atan2(y,x);
	*radius = sqrt(x*x + y*y);
}