#include "stretchy_buffer.h"

#include "raylib.h"

#include <math.h>
#include <stdint.h>
#include <stdio.h>

#define M_PI 3.14159265358979323846

typedef struct _Stick_pos_t {
  uint16_t x;
  uint16_t y;
  float radius;
  float angle;
  uint8_t visible;
} Stick_pos_t;

#define DEADZONE_RADIUS   100
#define RADIUS_OUTER      300
const float SCALE_FACTOR = RADIUS_OUTER / 255.0f;


void DrawDividers();

static const int screenWidth = 800;
static const int screenHeight = 800;

int main()
{
  InitWindow(screenWidth, screenHeight, "raylib [core] example - basic window");

  SetTargetFPS(30);

  FILE *f_data;
  char line[64];
  Stick_pos_t *records = NULL;
  Stick_pos_t sp = {};

  f_data = fopen("stick_pos.csv", "r");

  while (fgets(line, 64, f_data)) {
    float numbers[4];
    char *element_ptr_start = line;
    char *element_ptr_end = NULL;

    for (int el = 0; el < 4; el++) {
      numbers[el] = strtof(element_ptr_start, &element_ptr_end);
      element_ptr_start = element_ptr_end;
    }

    sp.x = ((numbers[0] - 127) * 2*SCALE_FACTOR) + (screenWidth / 2.0f);
    sp.y = ((numbers[1] - 127) * 2*SCALE_FACTOR) + (screenHeight / 2.0f);
    sp.radius = numbers[2];
    sp.angle = numbers[3];
    sp.visible = 0;

    sb_push(records, sp);
  }

  uint32_t records_count = sb_count(records);
  int32_t visible_start_idx = 0;
  int32_t visible_tail_idx = records_count;


  // Main game loop
  while (!WindowShouldClose()) // Detect window close button or ESC key
  {
    BeginDrawing();
    ClearBackground(DARKGRAY);

    DrawRectangle((screenWidth/2) - RADIUS_OUTER, (screenHeight/2) - RADIUS_OUTER,
                  2 * RADIUS_OUTER, 2 * RADIUS_OUTER, (Color){180, 180, 180, 255});
    DrawCircle(400, 400, RADIUS_OUTER, (Color){180, 180, 180, 255});
    //DrawCircle(400, 400, 200, (Color){160, 160, 160, 255});
    DrawCircle(400, 400, DEADZONE_RADIUS * SCALE_FACTOR, (Color){150, 150, 150, 255});

    DrawDividers();


    for (uint32_t point_idx = visible_start_idx; point_idx < visible_tail_idx; point_idx++) {

      DrawCircle(records[point_idx].x, records[point_idx].y, 3, RED);

      if (point_idx > 0) {
        DrawLineEx(
            (Vector2){records[point_idx - 1].x, records[point_idx - 1].y},
            (Vector2){records[point_idx].x, records[point_idx].y}, 2, RED);
      }
    }

    char text_buffer[128] = {};
    sprintf(text_buffer, "Draw range: [%d:%d]",
            visible_start_idx, visible_tail_idx);
    DrawText(text_buffer, 20, 20, 20, WHITE);
    if (IsKeyDown(KEY_D)) visible_start_idx--;
    if (IsKeyDown(KEY_F) && visible_start_idx < visible_tail_idx) visible_start_idx++;
    if (IsKeyDown(KEY_J) && visible_tail_idx > visible_start_idx) visible_tail_idx--;
    if (IsKeyDown(KEY_K)) visible_tail_idx++;
    if (visible_start_idx < 0) visible_start_idx = 0;
    if (visible_tail_idx > records_count) visible_tail_idx = records_count;
    if (visible_tail_idx < 0) visible_tail_idx = 0;


    if (IsKeyPressed(KEY_B)) {
      volatile static uint16_t dummy = 0;
      dummy++;
      visible_start_idx = visible_start_idx * dummy;
    }

    DrawRectangle(10, 800 - 30, 800 - 20, 20, LIGHTGRAY);
    uint16_t bar_start = 14;
    uint16_t bar_length = screenWidth - 2 * 14;
    float bar_active_start = ((float)visible_start_idx / records_count);
    float bar_active_end = ((float)visible_tail_idx / records_count);
    DrawRectangle(bar_start + bar_active_start * bar_length, 800 - 26,
                  (bar_active_end * bar_length) - bar_active_start * bar_length,
                  12, WHITE);

    EndDrawing();
    
    static uint16_t screenshot_idx = 0;
    char screenshot_file_name[32];
    sprintf(screenshot_file_name, "screen_%04d.png", screenshot_idx++);
    if (IsKeyPressed(KEY_P)) TakeScreenshot(screenshot_file_name);
  }

  fclose(f_data);
  CloseWindow(); // Close window and OpenGL context

  return 0;
}



void DrawDividers()
{
  float section_points[8][2] = {};
  section_points[0][0] = (RADIUS_OUTER * cosf(0.3926990817f)) + screenWidth/2;
  section_points[0][1] = screenHeight/2.0f - (RADIUS_OUTER * sinf(0.3926990817f));
  section_points[1][0] = (RADIUS_OUTER * cosf(1.178097f)) + screenWidth/2;
  section_points[1][1] = screenHeight/2.0f - (RADIUS_OUTER * sinf(1.178097f));
  section_points[2][0] = (RADIUS_OUTER * cosf(1.96349541f)) + screenWidth/2;
  section_points[2][1] = screenHeight/2.0f - (RADIUS_OUTER * sinf(1.96349541f));
  section_points[3][0] = (RADIUS_OUTER * cosf(2.74889357f)) + screenWidth/2;
  section_points[3][1] = screenHeight/2.0f - (RADIUS_OUTER * sinf(2.74889357f));

  section_points[4][0] = section_points[0][0];
  section_points[4][1] = screenHeight/2.0f + (RADIUS_OUTER * sinf(0.3926990817f));
  section_points[5][0] = section_points[1][0];
  section_points[5][1] = screenHeight/2.0f + (RADIUS_OUTER * sinf(1.178097f));
  section_points[6][0] = section_points[2][0];
  section_points[6][1] = screenHeight/2.0f + (RADIUS_OUTER * sinf(1.96349541f));
  section_points[7][0] = section_points[3][0];
  section_points[7][1] = screenHeight/2.0f + (RADIUS_OUTER * sinf(2.74889357f));

  DrawLineEx((Vector2){screenWidth / 2, screenHeight / 2},
             (Vector2){section_points[0][0], section_points[0][1]},
             2, GOLD);
  DrawLineEx((Vector2){screenWidth / 2, screenHeight / 2},
             (Vector2){section_points[1][0], section_points[1][1]},
             2, GOLD);
  DrawLineEx((Vector2){screenWidth / 2, screenHeight / 2},
             (Vector2){section_points[2][0], section_points[2][1]},
             2, GOLD);
  DrawLineEx((Vector2){screenWidth / 2, screenHeight / 2},
             (Vector2){section_points[3][0], section_points[3][1]},
             2, GOLD);
  
  DrawLineEx((Vector2){screenWidth / 2, screenHeight / 2},
             (Vector2){section_points[4][0], section_points[4][1]},
             2, GOLD);
  DrawLineEx((Vector2){screenWidth / 2, screenHeight / 2},
             (Vector2){section_points[5][0], section_points[5][1]},
             2, GOLD);
  DrawLineEx((Vector2){screenWidth / 2, screenHeight / 2},
             (Vector2){section_points[6][0], section_points[6][1]},
             2, GOLD);
  DrawLineEx((Vector2){screenWidth / 2, screenHeight / 2},
             (Vector2){section_points[7][0], section_points[7][1]},
             2, GOLD);
}