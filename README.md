---
tags: ["usb"]
---

# bluewii-host

- `kernel_space` - that was an exploration into making a kernel space drivers.
  - `linuxdriver` - kernel space driver for Linux. Didn't get very far.

- `user_space` - making a driver in the user space. Mostly for Windows
  - `libusbapp` - based on the `libusb`. It didn't get far. This library is to generic. From this one I went to `hidapi`.
  - `hidapiapp` - based on the `hidapi` library. **This acutally works**.
  - `winusb` - 
  - `winusbhid` - this basically tries to do the same as `hidapi` but through Windows sys calls.
  - `X` - there is nothing USB in this one. It's just input for xorg.
